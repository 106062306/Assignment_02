var bulletTime = 0;
var helperBulletTime = 0;
var bossBulletTime_1 = 0;
var firingTimer = 0;
var playerHP = 2000;
var bossHP = 5000;
var bossSkill_1 = false;
var livingEnemies_1 = [];
var livingEnemies_2 = [];
var position_x, position_y;
var killed_1 = 0;
var killed_2 = 0;
var score = 0;
var enableHelper = true;
var startHelper = false;
var menuState = {
    ready: false,
    preload: function() {
        game.load.image('menu_background', 'assets/menu_background.png');
        //game.load.image('game_background', 'assets/game_background_.jpg');
        game.load.image('game_background', 'assets/game_background.jpg');
        game.load.image('second_background', 'assets/second_background.jpg');
        game.load.image('boss_background', 'assets/boss_background.jpg');
        game.load.image('start', 'assets/play-button.png');
        game.load.image('about', 'assets/light-bulb.png');
        game.load.audio('bgm', 'assets/bgm.mp3');
        game.load.image('board', 'assets/board.png');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'menu_background');
        this.start = game.add.sprite(game.width/2-30, game.height/2+70, 'start');
        this.about = game.add.sprite(game.width/2-30, game.height/2+140, 'about');
        this.board = game.add.sprite(game.width/2-30, game.height/2+210, 'board');
        this.board.scale.x = 0.07;
        this.board.scale.y = 0.07;
        this.board.inputEnabled = true;
        this.board.events.onInputDown.add(this.clickToBoard, this);
        this.start.scale.x = 0.07;
        this.start.scale.y = 0.07;
        this.about.scale.x = 0.07;
        this.about.scale.y = 0.07;
        this.start.inputEnabled = true;
        this.start.events.onInputDown.add(this.clickToStart, this);
        this.about.inputEnabled = true;
        this.about.events.onInputDown.add(this.clickToAbout, this);
    },
    update: function() {
        
    },
    clickToStart: function() {
        game.state.start('first');
    },
    clickToAbout: function() {
        game.state.start('about');
    },
    clickToBoard: function() {
        game.state.start('board');
    }
}
var board = {
    preload: function() {
        game.load.image('board', 'assets/rank.png');
        game.load.image('menu', 'assets/menu.png');
    },
    create: function() {
        game.add.image(0, 0, 'board');
        this.menu = game.add.sprite(game.width/2-30, game.height/2+170, 'menu');
        this.menu.scale.x = 0.07;
        this.menu.scale.y = 0.07;
        this.menu.inputEnabled = true;
        this.menu.events.onInputDown.add(this.clickToMenu, this);
        var postsRef = firebase.database().ref('rank').orderByChild('score').limitToLast(5);
        var name = [];
        var score = [];
        var spacing=0;
        var rank=1;
        postsRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function(childSnapshot) {
                    name.push(childSnapshot.val().name);
                    score.push(childSnapshot.val().score);
                });
                for(let i = 4; i >= 0; i--){
                    game.add.text(100, 240 + spacing, "#" + rank + ". " + name[i] + " " + score[i], {font: '30px monospace', fill: '#cfe833', align: 'center'})
                    spacing += 40;
                    rank += 1;
                }
            })
            .catch(e => console.log(e.message));
    },
    clickToMenu: function() {
        game.state.start('menu');
    }
}
var gameOver = {
    preload: function() {
        game.load.image('start', 'assets/play-button.png');
        game.load.image('dead', 'assets/dead.png');
        game.load.image('board', 'assets/board.png');
    },
    create: function() {
        game.add.image(0, 0, 'dead');
        this.text = game.add.text(game.world.centerX-60, game.world.centerY-25, "You Lose.", {
            font: "25px Courier New",
            fill: "#ff9400",
            align: "center"
        });
        this.start = game.add.sprite(game.width/2-30, game.height/2+90, 'start');
        this.board = game.add.sprite(game.width/2-30, game.height/2+170, 'board');
        this.start.scale.x = 0.07;
        this.start.scale.y = 0.07;
        this.board.scale.x = 0.07;
        this.board.scale.y = 0.07;
        this.start.inputEnabled = true;
        this.start.events.onInputDown.add(this.clickToStart, this);
        this.board.inputEnabled = true;
        this.board.events.onInputDown.add(this.clickToBoard, this);
        bulletTime = 0;
        helperBulletTime = 0;
        bossBulletTime_1 = 0;
        firingTimer = 0;
        playerHP = 2000;
        bossHP = 5000;
        bossSkill_1 = false;
        livingEnemies_1 = [];
        livingEnemies_2 = [];
        position_x, position_y;
        killed_1 = 0;
        killed_2 = 0;
        enableHelper = true;
        startHelper = false;
        var name = prompt("Please enter your name");
        if (name != null) {
            firebase.database().ref('rank').push({
                score:score,
                name:name
            });
        }
        score = 0;
    },
    clickToStart: function() {
        game.state.start('first');
    },
    clickToBoard: function() {
        game.state.start('board');
    }
}
var win = {
    preload: function() {
        game.load.image('start', 'assets/play-button.png');
        game.load.image('win', 'assets/win.png');
        game.load.image('board', 'assets/board.png');
    },
    create: function() {
        game.add.image(0, 0, 'win');
        this.text = game.add.text(game.world.centerX-60, game.world.centerY-25, "You Win!", {
            font: "25px Courier New",
            fill: "#ff9400",
            align: "center"
        });
        this.start = game.add.sprite(game.width/2-30, game.height/2+90, 'start');
        this.board = game.add.sprite(game.width/2-30, game.height/2+170, 'board');
        this.board.scale.x = 0.07;
        this.board.scale.y = 0.07;
        this.board.inputEnabled = true;
        this.board.events.onInputDown.add(this.clickToBoard, this);
        bulletTime = 0;
        this.start.scale.x = 0.07;
        this.start.scale.y = 0.07;
        this.start.inputEnabled = true;
        this.start.events.onInputDown.add(this.clickToStart, this);
        bulletTime = 0;
        helperBulletTime = 0;
        bossBulletTime_1 = 0;
        firingTimer = 0;
        playerHP = 2000;
        bossHP = 5000;
        bossSkill_1 = false;
        livingEnemies_1 = [];
        livingEnemies_2 = [];
        position_x, position_y;
        killed_1 = 0;
        killed_2 = 0;
        enableHelper = true;
        startHelper = false;
        var name = prompt("Please enter your name");
        if (name != null) {
            firebase.database().ref('rank').push({
                score:score,
                name:name
            });
        }
        score = 0;
    },
    clickToStart: function() {
        game.state.start('first');
    },
    clickToBoard: function() {
        game.state.start('board');
    }
}
var aboutState = {
    preload: function() {
        game.load.image('about_img', 'assets/about.png');
        game.load.image('menu', 'assets/menu.png');
    },
    create: function() {
        game.add.image(0, 0, 'about_img');
        this.menu = game.add.sprite(game.width/2-30, game.height/2+190, 'menu');
        this.text = game.add.text(game.world.centerX-280, game.world.centerY-25, "Hello Heroes \n\n There three levels you need to pass \n\n Prepare your bullet, let's fight!", {
            font: "25px Courier New",
            fill: "#ff9400",
            align: "center"
        });
        this.menu.scale.x = 0.07;
        this.menu.scale.y = 0.07;
        this.menu.inputEnabled = true;
        this.menu.events.onInputDown.add(this.clickToMenu, this);
    },
    clickToMenu: function() {
        game.state.start('menu');
    }
}
var firstLevel = {
    preload: function() {
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet', 'assets/bullet_1.png');
        game.load.image('enemy', 'assets/monster.png', 100, 100);
        game.load.image('helper_pre', 'assets/helper.png');
        game.load.image('helper', 'assets/helper.png');
        game.load.image('enemyB', 'assets/enemy_2.png');

        game.load.spritesheet('player', 'assets/jet.png', 40, 48);

        ///load sound effect
        game.load.audio('bullet_sound', 'assets/bullet_sound.wav');
        game.load.audio('explosion_sound', 'assets/explosion_sound.wav');

    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'game_background'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'game_background');
        this.bgm = game.sound.play('bgm');
        this.bgm.loopFull(0.6);
        this.text = game.add.text(game.world.centerX-280, game.world.centerY+230, "Score: " + score, {
            font: "25px Courier New",
            fill: "#000000",
            align: "center"
        });

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();

        
        this.player = game.add.sprite(game.width/2, game.height/2 + 200, 'player');
        //this.player.facingLeft = false;
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;

        this.player.animations.add('right', [1, 2], 8, true);
        this.player.animations.add('left', [1, 2], 8, true);
        
        ///Add helper
        this.helper = game.add.sprite(10000, 0, 'helper');
        //this.player.facingLeft = false;
        game.physics.arcade.enable(this.helper);
        

        ///Add helper bullet
        this.weapon_h = game.add.group();
        this.weapon_h.enableBody = true;
        this.weapon_h.physicsBodyType = Phaser.Physics.ARCADE;
        this.weapon_h.createMultiple(30, 'bullet');
        this.weapon_h.setAll('anchor.x', 0.5);
        this.weapon_h.setAll('anchor.y', 1);
        this.weapon_h.setAll('outOfBoundsKill', true);
        this.weapon_h.setAll('checkWorldBounds', true);

        // this.HP = new Phaser.Rectangle(300, 80, playerHP, 30);
        ///Add bullet
        this.weapon = game.add.group();
        this.weapon.enableBody = true;
        this.weapon.physicsBodyType = Phaser.Physics.ARCADE;
        this.weapon.createMultiple(30, 'bullet');
        this.weapon.setAll('anchor.x', 0.5);
        this.weapon.setAll('anchor.y', 1);
        this.weapon.setAll('outOfBoundsKill', true);
        this.weapon.setAll('checkWorldBounds', true);
        this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        ///add bullet sound
        this.bullet_sound = game.add.audio('bullet_sound');
        ///Add enemies
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.createMultiple(30, 'enemy');
        this.enemies.setAll('anchor.x', 0.5);
        this.enemies.setAll('anchor.y', 0.5);
        this.enemies.setAll('outOfBoundsKill', true);
        this.enemies.setAll('checkWorldBounds', true);
        this.nextEnemyAt = 0;
        this.enemyDelay = 2000;
        ///Add helper
        this.helper_pre = game.add.group();
        this.helper_pre.enableBody = true;
        this.helper_pre.createMultiple(1, 'helper_pre');
        this.helper_pre.setAll('anchor.x', 0.5);
        this.helper_pre.setAll('anchor.y', 0.5);
        this.helper_pre.setAll('outOfBoundsKill', true);
        this.helper_pre.setAll('checkWorldBounds', true);
        this.nextHelperAt = 10000;
        this.helperDelay = 100000;
        ///Add enemies' bullet
        this.enemyWeapon = game.add.group();
        this.enemyWeapon.enableBody = true;
        this.enemyWeapon.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyWeapon.createMultiple(30, 'enemyB');
        this.enemyWeapon.setAll('anchor.x', 0.5);
        this.enemyWeapon.setAll('anchor.y', 1);
        this.enemyWeapon.setAll('outOfBoundsKill', true);
        this.enemyWeapon.setAll('checkWorldBounds', true);
        ///add explosion sound effect
        this.explosion_sound = game.add.audio('explosion_sound');
        ///Lives
        this.lives = game.add.group();
        //this.bgm = new Phaser.Sound(game, 'bgm', 1, true);
        game.sound.setDecodedCallback([this.bullet_sound, this.explosion_sound], start, this);
        
    },
    descend: function() {
        this.enemies.y += 100;
    },

    blockParticle_p: function() {
        this.emitter_p.start(true, 800, null, 15);
    },
    blockParticle_e: function() {
        this.emitter_e.start(true, 800, null, 15);
    },
    update: function() {
        this.background.tilePosition.y += 2;
        this.HP = new Phaser.Rectangle(370, 30, playerHP * 0.1, 20);

        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();

        if(startHelper) {
            this.fireHelperBullet();
        }
        if(this.fireButton.isDown) {
            this.fireBullet();
            //this.weapon.body.velocity.y = 100;
        }
        if (game.time.now > firingTimer)
        {
            this.enemyFires();
        }
        if (this.nextEnemyAt < game.time.now && this.enemies.countDead()>0) {
            this.nextEnemyAt = game.time.now + this.enemyDelay;
            var enemy = this.enemies.getFirstExists(false);
            enemy.reset(game.rnd.integerInRange(70, 530), 0);
            enemy.body.velocity.y = game.rnd.integerInRange(30, 60);
        }
        if (this.nextHelperAt < game.time.now && this.helper_pre.countDead()>0 && enableHelper) {
            this.nextHelperAt = game.time.now + this.helperDelay;
            var helper_pre_ = this.helper_pre.getFirstExists(false);
            helper_pre_.reset(game.rnd.integerInRange(65, 530), 0);
            helper_pre_.body.velocity.y = game.rnd.integerInRange(30, 60);
        }
        if(killed_1 == 10) game.state.start('second');
        if(playerHP < 10) game.state.start('over');
        position_x = this.player.body.x;
        position_y = this.player.body.y;
        game.physics.arcade.overlap(this.player, this.helper_pre, this.getHelper, null, this);
        game.physics.arcade.overlap(this.player, this.enemies, this.objectCollision, null, this);
        game.physics.arcade.overlap(this.helperBullet, this.enemies, this.helpKill, null, this);
        game.physics.arcade.overlap(this.weapon, this.enemies, this.collisionHandler, null, this);
        game.physics.arcade.overlap(this.enemyWeapon, this.player, this.enemyHitsPlayer, null, this);
    },
    helpKill: function(helperBullet_, enemy_) {
        this.emitter_e = game.add.emitter(enemy_.body.x, enemy_.body.y, 15);
        this.emitter_e.makeParticles('pixel');
        this.emitter_e.setYSpeed(-150, 150);
        this.emitter_e.setXSpeed(-150, 150);
        this.emitter_e.setScale(2, 0, 2, 0, 800);
        this.emitter_e.gravity = 500;
        game.physics.arcade.collide(enemy_, helperBullet_, this.blockParticle_e, null, this);
        helperBullet_.kill();
        enemy_.kill();
        killed_1++;
        score += 10;
        this.text.text = "Score: " + score;
    },
    getHelper: function() {
        this.helper_pre.kill();
        enableHelper = false;
        this.helper.body.x = this.player.body.x + 40;
        this.helper.body.y = this.player.body.y + 10;
        this.helper.body.collideWorldBounds = true;
        startHelper = true;
    },
    objectCollision: function() {
        playerHP -= 1;
    },
    fireHelperBullet: function() {
        if(game.time.now > helperBulletTime) {
            this.helperBullet = this.weapon_h.getFirstExists(false);
            if(this.helperBullet) {
                this.helperBullet.reset(this.helper.x + 30, this.helper.y + 8);
                this.helperBullet.body.velocity.y = -400;
                helperBulletTime = game.time.now + 1000;
            }
        }
    },
    fireBullet: function() {
        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            this.weapon_ = this.weapon.getFirstExists(false);
            this.playerFire();
            if (this.weapon_)
            {
                //  And fire it
                this.weapon_.reset(this.player.x, this.player.y + 8);
                this.weapon_.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
            }
        }
    },
    playerFire: function() {
        console.log("HERE4");
        this.bullet_sound.play();
    },
    enemyHitsPlayer: function(player, enemyWeapon_) {
    
        this.emitter_p = game.add.emitter(this.player.body.x, this.player.body.y, 15);
        this.emitter_p.makeParticles('pixel');
        this.emitter_p.setYSpeed(-150, 150);
        this.emitter_p.setXSpeed(-150, 150);
        this.emitter_p.setScale(2, 0, 2, 0, 800);
        this.emitter_p.gravity = 500;
        game.physics.arcade.collide(this.player, enemyWeapon_, this.blockParticle_p, null, this);
        enemyWeapon_.kill();
        playerHP -= 100;
        console.log(playerHP);
    
    },
    enemyFires: function() {

        //  Grab the first bullet we can from the pool
        this.enemyWeapon__ = this.enemyWeapon.getFirstExists(false);
    
        livingEnemies_1.length=0;
    
        this.enemies.forEachAlive(function(enemy){
            // put every living enemy in an array
            livingEnemies_1.push(enemy);
        });
    
        if (this.enemyWeapon__ && livingEnemies_1.length > 0)
        {
            
            var random = game.rnd.integerInRange(0, livingEnemies_1.length-1);
    
            // randomly select one of them
            var shooter = livingEnemies_1[random];
            // And fire the bullet from this enemy
            this.enemyWeapon__.reset(shooter.body.x + 34, shooter.body.y + 100);
    
            game.physics.arcade.moveToObject(this.enemyWeapon__, this.player, 120);
            firingTimer = game.time.now + 2000;
        }
    
    },
    collisionHandler: function(weapon_, enemy_) {

        //  When a bullet hits an alien we kill them both
        this.emitter_e = game.add.emitter(enemy_.body.x, enemy_.body.y, 15);
        this.emitter_e.makeParticles('pixel');
        this.emitter_e.setYSpeed(-150, 150);
        this.emitter_e.setXSpeed(-150, 150);
        this.emitter_e.setScale(2, 0, 2, 0, 800);
        this.emitter_e.gravity = 500;
        game.physics.arcade.collide(enemy_, weapon_, this.blockParticle_e, null, this);
        weapon_.kill();
        enemy_.kill();
        killed_1++;
        score += 10;
        this.text.text = "Score: " + score;
        ///add sound effect
        this.hitEnemy();
    
    },
    hitEnemy: function() {
        this.explosion_sound.play();
    },
    playerDie: function() { game.state.start('main');},
    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            if(startHelper) this.helper.body.velocity.x = -200;
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('left');
            ///
        }
        else if (this.cursor.right.isDown) { 
            if(startHelper) this.helper.body.velocity.x = 200;
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('right');
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
            if(startHelper) this.helper.body.velocity.y = -350;
            this.player.body.velocity.y = -350;
        }
        else if(this.cursor.down.isDown) {
            if(startHelper) this.helper.body.velocity.y = 350;
            this.player.body.velocity.y = 350;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            if(startHelper) {
                this.helper.body.velocity.x = 0;
                this.helper.body.velocity.y = 0;
            }
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 3;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    },
    render: function() {
        game.debug.geom(this.HP, '#0fffff');
    }
};
var secondLevel = {
    preload: function() {

        // Loat game sprites.
        
        //game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet', 'assets/bullet_2.png', 5, 5);
        game.load.image('enemy', 'assets/monster2.png', 100, 100);
        game.load.image('helper', 'assets/helper.png');
        game.load.image('enemyB', 'assets/enemy_1.png');

        game.load.spritesheet('player', 'assets/jet.png', 40, 48);

        ///load sound effect
        game.load.audio('bullet_sound', 'assets/bullet_sound.wav');
        game.load.audio('explosion_sound', 'assets/explosion_sound.wav');

    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'second_background'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'second_background');
        // this.bgm = game.sound.play('bgm');
        // this.bgm.loopFull(0.6);
        this.text = game.add.text(game.world.centerX-280, game.world.centerY+230, "Score: " + score, {
            font: "25px Courier New",
            fill: "#ffffff",
            align: "center"
        });

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();

        
        this.player = game.add.sprite(position_x, position_y, 'player');
        //this.player.facingLeft = false;
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;

        this.player.animations.add('right', [1, 2], 8, true);
        this.player.animations.add('left', [1, 2], 8, true);

        this.helper = game.add.sprite(this.player.body.x, this.player.body.y, 'helper');
        game.physics.arcade.enable(this.helper);
        this.helper.body.collideWorldBounds = true;

        ///Add bullet
        this.weapon = game.add.group();
        this.weapon.enableBody = true;
        this.weapon.physicsBodyType = Phaser.Physics.ARCADE;
        this.weapon.createMultiple(30, 'bullet');
        this.weapon.setAll('anchor.x', 0.5);
        this.weapon.setAll('anchor.y', 1);
        this.weapon.setAll('outOfBoundsKill', true);
        this.weapon.setAll('checkWorldBounds', true);
        this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        ///add helper bullet
        this.weapon_h = game.add.group();
        this.weapon_h.enableBody = true;
        this.weapon_h.physicsBodyType = Phaser.Physics.ARCADE;
        this.weapon_h.createMultiple(30, 'bullet');
        this.weapon_h.setAll('anchor.x', 0.5);
        this.weapon_h.setAll('anchor.y', 1);
        this.weapon_h.setAll('outOfBoundsKill', true);
        this.weapon_h.setAll('checkWorldBounds', true);
        ///add bullet sound
        this.bullet_sound = game.add.audio('bullet_sound');
        ///Add enemies
        this.enemies = game.add.group();
        this.enemies.enableBody = true;
        this.enemies.physicsBodyType = Phaser.Physics.ARCADE;
        this.createEnemies();
        ///Add enemies' bullet
        this.enemyWeapon = game.add.group();
        this.enemyWeapon.enableBody = true;
        this.enemyWeapon.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyWeapon.createMultiple(30, 'enemyB');
        this.enemyWeapon.setAll('anchor.x', 0.5);
        this.enemyWeapon.setAll('anchor.y', 1);
        this.enemyWeapon.setAll('outOfBoundsKill', true);
        this.enemyWeapon.setAll('checkWorldBounds', true);
        this.explosion_sound = game.add.audio('explosion_sound');
        ///Lives
        this.lives = game.add.group();
        
        //this.bgm = new Phaser.Sound(game, 'bgm', 1, true);
        game.sound.setDecodedCallback([this.bullet_sound, this.explosion_sound], start, this);
        
    },
    fireHelperBullet: function() {
        if(game.time.now > helperBulletTime) {
            console.log("12345");
            this.helperBullet = this.weapon_h.getFirstExists(false);
            if(this.helperBullet) {
                this.helperBullet.reset(this.helper.x + 30, this.helper.y + 8);
                this.helperBullet.body.velocity.y = -400;
                helperBulletTime = game.time.now + 1000;
            }
        }
    },
    createEnemies: function() {
        for(let i = 0; i < 4; ++i){
            for(let j = 0; j < 8; ++j){
                this.enemy = this.enemies.create(j * 60, i * 60, 'enemy');
                this.enemy.anchor.setTo(0.5, 0.5);
            }
        }
        this.enemies.x = 100;
        this.enemies.y = 50;
        this.tween_ = game.add.tween(this.enemies).to({x: 100 + game.rnd.integerInRange(-30, 30), y: 200 + game.rnd.integerInRange(-30, 30)}, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        this.tween_.onLoop.add(this.descend, this);
    },
    descend: function() {
        this.enemies.y += 100;
    },

    blockParticle_p: function() {
        this.emitter_p.start(true, 800, null, 15);
    },
    blockParticle_e: function() {
        this.emitter_e.start(true, 800, null, 15);
    },
    update: function() {
        //this.bgm.play();
        this.fireHelperBullet();
        this.background.tilePosition.y += 2;
        this.HP = new Phaser.Rectangle(370, 30, playerHP * 0.1, 20);

        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();

        if(this.fireButton.isDown) {
            this.fireBullet();
            //this.weapon.body.velocity.y = 100;
        }
        if (game.time.now > firingTimer)
        {
            this.enemyFires();
        }

        if(killed_2 == 32) {
            game.state.start('boss');
            startHelper = false;
        }
        if(playerHP < 10) game.state.start('over');
        game.physics.arcade.overlap(this.player, this.helper_pre, this.getHelper, null, this);
        game.physics.arcade.overlap(this.player, this.enemies, this.objectCollision, null, this);
        game.physics.arcade.overlap(this.weapon, this.enemies, this.collisionHandler, null, this);
        game.physics.arcade.overlap(this.enemyWeapon, this.player, this.enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(this.helperBullet, this.enemies, this.helpKill, null, this);
    },
    helpKill: function(helperBullet_, enemy_) {
        this.emitter_e = game.add.emitter(enemy_.body.x, enemy_.body.y, 15);
        this.emitter_e.makeParticles('pixel');
        this.emitter_e.setYSpeed(-150, 150);
        this.emitter_e.setXSpeed(-150, 150);
        this.emitter_e.setScale(2, 0, 2, 0, 800);
        this.emitter_e.gravity = 500;
        game.physics.arcade.collide(enemy_, helperBullet_, this.blockParticle_e, null, this);
        helperBullet_.kill();
        enemy_.kill();
        killed_2++;
        score += 10;
        this.text.text = "Score: " + score;
    },
    fireBullet: function() {
        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            this.weapon_ = this.weapon.getFirstExists(false);
            this.playerFire();
            if (this.weapon_ && livingEnemies_2.length > 0)
            {
                //  And fire it
                for(let i = 0; i < livingEnemies_2.length; ++i) {
                    var shooter = livingEnemies_2[i];
                    this.weapon_.reset(this.player.x, this.player.y + 8);
                    this.weapon_.body.velocity.y = -400;
                    bulletTime = game.time.now + 200;
                    game.physics.arcade.moveToObject(this.weapon_, shooter, 1000);
                }
            }
        }
    },
    playerFire: function() {
        console.log("HERE4");
        this.bullet_sound.play();
    },
    enemyHitsPlayer: function(player, enemyWeapon_) {
    
        this.emitter_p = game.add.emitter(this.player.body.x, this.player.body.y, 15);
        this.emitter_p.makeParticles('pixel');
        this.emitter_p.setYSpeed(-150, 150);
        this.emitter_p.setXSpeed(-150, 150);
        this.emitter_p.setScale(2, 0, 2, 0, 800);
        this.emitter_p.gravity = 500;
        game.physics.arcade.collide(this.player, enemyWeapon_, this.blockParticle_p, null, this);
        enemyWeapon_.kill();
        playerHP -= 100;
        console.log(playerHP);
    
    },
    enemyFires: function() {

        //  Grab the first bullet we can from the pool
        this.enemyWeapon__ = this.enemyWeapon.getFirstExists(false);
    
        livingEnemies_2.length=0;
    
        this.enemies.forEachAlive(function(enemy){
            // put every living enemy in an array
            livingEnemies_2.push(enemy);
        });
    
        if (this.enemyWeapon__ && livingEnemies_2.length > 0)
        {
            
            var random = game.rnd.integerInRange(0, livingEnemies_2.length-1);
    
            // randomly select one of them
            var shooter = livingEnemies_2[random];
            // And fire the bullet from this enemy
            this.enemyWeapon__.reset(shooter.body.x, shooter.body.y);
    
            game.physics.arcade.moveToObject(this.enemyWeapon__, this.player, 120);
            firingTimer = game.time.now + 2000;
        }
    
    },
    collisionHandler: function(weapon_, enemy_) {

        //  When a bullet hits an alien we kill them both
        this.emitter_e = game.add.emitter(enemy_.body.x, enemy_.body.y, 15);
        this.emitter_e.makeParticles('pixel');
        this.emitter_e.setYSpeed(-150, 150);
        this.emitter_e.setXSpeed(-150, 150);
        this.emitter_e.setScale(2, 0, 2, 0, 800);
        this.emitter_e.gravity = 500;
        game.physics.arcade.collide(enemy_, weapon_, this.blockParticle_e, null, this);
        weapon_.kill();
        enemy_.kill();
        killed_2++;
        score += 50;
        this.text.text = "Score: " + score;
        ///add sound effect
        this.hitEnemy();
    
    },
    hitEnemy: function() {
        this.explosion_sound.play();
    },
    playerDie: function() { game.state.start('main');},
    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.helper.body.velocity.x = -200;
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('left');
            ///
        }
        else if (this.cursor.right.isDown) { 
            this.helper.body.velocity.x = 200;
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('right');
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
            this.helper.body.velocity.y = -350;
            this.player.body.velocity.y = -350;
        }
        else if(this.cursor.down.isDown) {
            this.helper.body.velocity.y = 350;
            this.player.body.velocity.y = 350;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.helper.body.velocity.x = 0;
            this.helper.body.velocity.y = 0;
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 3;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    },
    render: function() {
        game.debug.geom(this.HP, '#0fffff');
    }
};
var bossLevel = {
    preload: function() {

        // Loat game sprites.
        
        //game.load.image('ground', 'assets/ground.png');
        game.load.image('pixel', 'assets/pixel.png');
        game.load.image('bullet', 'assets/hamburger.png', 5, 5);
        game.load.image('bulletB', 'assets/pizza.png', 5, 5);
        game.load.image('enemy', 'assets/monster.png', 100, 100);
        game.load.image('boss', 'assets/boss.png', 100, 100);
        game.load.image('flash', 'assets/flash.png');
        game.load.image('playerSkill', 'assets/playerSkill.png');

        game.load.spritesheet('player', 'assets/jet.png', 40, 48);


        ///load sound effect
        game.load.audio('bullet_sound', 'assets/bullet_sound.wav');
        game.load.audio('explosion_sound', 'assets/explosion_sound.wav');

    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'boss_background'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'boss_background');
        // this.bgm = game.sound.play('bgm');
        // this.bgm.loopFull(0.6);
        this.text = game.add.text(game.world.centerX-280, game.world.centerY+230, "Score: " + score, {
            font: "25px Courier New",
            fill: "#ffffff",
            align: "center"
        });

        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        ///

        game.renderer.renderSession.roundPixels = true;

        this.cursor = game.input.keyboard.createCursorKeys();

        
        this.player = game.add.sprite(game.width/2, game.height/2 + 200, 'player');
        //this.player.facingLeft = false;
        game.physics.arcade.enable(this.player);
        this.player.body.collideWorldBounds = true;

        this.player.animations.add('right', [1, 2], 8, true);
        this.player.animations.add('left', [1, 2], 8, true);
        ///Add boss
        this.boss = game.add.sprite(game.width/2, game.height/2 + 60, 'boss');
        //this.boss.anchor.setTo(0.5, 0.5);
        game.physics.arcade.enable(this.boss);
        this.tween__ = game.add.tween(this.boss).to({x: 100, y: 0}, 3000, Phaser.Easing.Linear.None, true, 0, 1000, true);
        this.boss.body.collideWorldBounds = true;
        this.tween__.onLoop.add(this.descend, this);
        ///Add bullet
        this.weapon = game.add.group();
        this.weapon.enableBody = true;
        this.weapon.physicsBodyType = Phaser.Physics.ARCADE;
        this.weapon.createMultiple(30, 'bullet');
        this.weapon.setAll('anchor.x', 0.5);
        this.weapon.setAll('anchor.y', 1);
        this.weapon.setAll('outOfBoundsKill', true);
        this.weapon.setAll('checkWorldBounds', true);
        this.fireButton = this.input.keyboard.addKey(Phaser.KeyCode.SPACEBAR);
        ///add bullet sound
        this.bullet_sound = game.add.audio('bullet_sound');
        ///Add enemies' bullet
        this.enemyWeapon_1 = game.add.group();
        this.enemyWeapon_1.enableBody = true;
        this.enemyWeapon_1.physicsBodyType = Phaser.Physics.ARCADE;
        this.enemyWeapon_1.createMultiple(30, 'bulletB');
        this.enemyWeapon_1.setAll('anchor.x', 0.5);
        this.enemyWeapon_1.setAll('anchor.y', 1);
        this.enemyWeapon_1.setAll('outOfBoundsKill', true);
        this.enemyWeapon_1.setAll('checkWorldBounds', true);
        //this.enemyWeapon_1.trackSprite(this.boss, 0, 0, true);

        ///add explosion sound effect
        this.explosion_sound = game.add.audio('explosion_sound');
        ///Lives
        this.lives = game.add.group();

        this.flash = game.add.sprite(1000, 0, 'flash');
        game.physics.arcade.enable(this.flash);

        this.pSkill = game.add.sprite(1000, 1000, 'playerSkill');
        game.physics.arcade.enable(this.pSkill);
        
        game.sound.setDecodedCallback([this.bullet_sound, this.explosion_sound], start, this);
        
    },
    objectCollision: function() {
        playerHP -= 10;
    },
    descend: function() {
        this.boss.y += 100;
    },
    blockParticle_p: function() {
        this.emitter_p.start(true, 800, null, 15);
    },
    blockParticle_e: function() {
        this.emitter_e.start(true, 800, null, 15);
    },
    update: function() {
        //this.bgm.play();
        this.background.tilePosition.y += 2;
        this.HP = new Phaser.Rectangle(370, 30, playerHP * 0.1, 20);
        this.bossHP = new Phaser.Rectangle(10, 30, bossHP * 0.05, 20);


        if (!this.player.inWorld) { this.playerDie();}
        this.movePlayer();

        if(this.fireButton.isDown) {
            this.fireBullet();
        }
        if (game.time.now > bossBulletTime_1)
        {
            this.enemyFires_1();
        }
        if(bossHP > 1800 && bossHP < 3500) {
            this.bossSkill_1();
        }
        if(bossHP <= 1800) {
            this.flash.kill();
        }
        if(bossHP <= 1000) this.boss.body.angularVelocity = -100;
        if(bossHP < 0) {
            this.boss.kill();
            console.log("KILLED");
            game.state.start('win');
        }
        if(playerHP < 400) {
            this.showPSkill();
        }
        if(playerHP < 0)
        {
            this.pSkill.kill();
            game.state.start('over');
        }
        position_x = this.player.body.x;
        position_y = this.player.body.y;
        game.physics.arcade.overlap(this.player, this.helper_pre, this.getHelper, null, this);
        game.physics.arcade.overlap(this.player, this.boss, this.objectCollision, null, this);
        game.physics.arcade.overlap(this.player, this.flash, this.flashKill, null, this);
        game.physics.arcade.overlap(this.boss, this.pSkill, this.playerSkill, null, this);
        game.physics.arcade.overlap(this.weapon, this.boss, this.collisionHandler, null, this);
        game.physics.arcade.overlap(this.enemyWeapon_1, this.player, this.enemyHitsPlayer, null, this);
    },
    showPSkill: function() {
        this.pSkill.body.x = this.player.body.x;
        this.pSkill.body.y = this.player.body.y - 250;
    },
    playerSkill: function() {
        bossHP -= 20;
        score += 10;
        this.text.text = "Score: " + score;
    },
    flashKill: function() {
        playerHP -= 10;
    },
    bossSkill_1: function() {
        this.flash.body.x = this.boss.body.x + 40;
        this.flash.body.y = this.boss.body.y + 126;
    },
    enemyFires_1: function() {
        if (game.time.now > bossBulletTime_1)
        {
            //  Grab the first bullet we can from the pool
            this.bossweapon_1 = this.enemyWeapon_1.getFirstExists(false);
            if (this.bossweapon_1)
            {
                //  And fire it
                this.bossweapon_1.reset(this.boss.body.x + 40, this.boss.body.y + 100);
                this.bossweapon_1.body.velocity.y = 400;
                bossBulletTime_1 = game.time.now + 200;
            }
        }
    },
    playerFire: function() {
        this.bullet_sound.play();
    },
    enemyHitsPlayer: function(player, enemyWeapon_) {
    
        this.emitter_p = game.add.emitter(this.player.body.x, this.player.body.y, 15);
        this.emitter_p.makeParticles('pixel');
        this.emitter_p.setYSpeed(-150, 150);
        this.emitter_p.setXSpeed(-150, 150);
        this.emitter_p.setScale(2, 0, 2, 0, 800);
        this.emitter_p.gravity = 500;
        game.physics.arcade.collide(this.player, enemyWeapon_, this.blockParticle_p, null, this);
        enemyWeapon_.kill();
        playerHP -= 100;
        console.log(playerHP);
    },
    fireBullet: function() {

        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            this.weapon_ = this.weapon.getFirstExists(false);
            this.playerFire();
            if (this.weapon_)
            {
                //  And fire it
                this.weapon_.reset(this.player.x, this.player.y + 8);
                this.weapon_.body.velocity.y = -400;
                bulletTime = game.time.now + 200;
            }
        }
    
    },
    collisionHandler: function(boss, weapon) {

        //  When a bullet hits an alien we kill them both
        this.emitter_e = game.add.emitter(this.boss.body.x, this.boss.body.y, 15);
        this.emitter_e.makeParticles('pixel');
        this.emitter_e.setYSpeed(-150, 150);
        this.emitter_e.setXSpeed(-150, 150);
        this.emitter_e.setScale(2, 0, 2, 0, 800);
        this.emitter_e.gravity = 500;
        game.physics.arcade.collide(this.boss, weapon, this.blockParticle_e, null, this);
        weapon.kill();
        bossHP -= 100;
        //  Increase the score
        score += 100;
        this.text.text = "Score: " + score;
        ///add sound effect
        //this.hitEnemy();
    
    },
    hitEnemy: function() {
        this.explosion_sound.play();
    },
    playerDie: function() { game.state.start('main');},
    /// ToDo 7: Finish the 4 animation part.
    movePlayer: function() {
        if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.facingLeft = true;

            /// 1. Play the animation 'leftwalk'
            this.player.animations.play('left');
            ///
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.facingLeft = false;

            /// 2. Play the animation 'rightwalk' 
            this.player.animations.play('right');
            ///
        }    

        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) { 
            this.player.body.velocity.y = -350;
        }
        else if(this.cursor.down.isDown) {
            this.player.body.velocity.y = 350;
        }
        // If neither the right or left arrow key is pressed
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
        
            if(this.player.facingLeft) {
                // Change player frame to 3 (Facing left)
                this.player.frame = 3;
            }else {
                // Change player frame to 1 (Facing right)
                this.player.frame = 3;
            }

            // Stop the animation
            this.player.animations.stop();
        }    
    },
    render: function() {
        game.debug.geom(this.HP, '#00ff87');
        game.debug.geom(this.bossHP, '#ff4300');
    }
};

var game = new Phaser.Game(600, 600, Phaser.AUTO, 'canvas');
game.state.add('menu', menuState);
game.state.add('about', aboutState);
game.state.add('first', firstLevel);
game.state.add('second', secondLevel);
game.state.add('boss', bossLevel);
game.state.add('over', gameOver);
game.state.add('win', win);
game.state.add('board', board);
game.state.start('menu');