# Software Studio 2019 Spring Assignment2

## Scoring
|Component|Score|
|:-:|:-:|
|Complete game process|15%|
|Basic rules|20%|
|Jucify mechanisms|15%|
|Animations / Particle Systems|each for 10%| 
|UI / Sound effects / Leaderboard|each for 5%|
|Appearance|5%|
|Bonus|40%|
  * Level:
    1. 總共有三關
    2. 第一關怪物是一隻一隻下來
    3. 第二關是一次一群怪獸
    4. 第三關是boss關
  * Skill:
    1. 在boss關 player血量 < 400時 會施放大絕
  * Animations:
    1. |Add animation to player|Y|
  * Particle Systems:
    1. |Add particle systems to player’s and enemy’s bullets|Y|
  * UI:
    1. |Player health|Y|
    2. |Ultimate skill number or power|N|
    3. |Score|Y|
    4. |volume control|N|
    5. |Game pause|N|
  * Sound effects:
    1. |shoot sound effect|Y|
    2. |bgm|Y|
    3. |can change volume|N|
  * Leaderboard:
    1. |firebase|Y|
    Multi-player game: X 
  * Enhanced items:
    1. |Bullet automatic aiming|5%|Y|
        * 第二關可自動瞄準敵人
    2. |Unique bullet|5%|Y|
        * 每過一關 enemy&player的子彈都會變
    3. |Little helper|5%|Y|
        * Little helper能在第一關獲得
        * 獲得後會放在player旁邊 每隔一段時間射出一發子彈
        * 可持續至第二關
        * 到boss關消失
  * Boss:
    1. |Unique movement and attack-mode|5%|Y|
  * Others:
    1. 每過一關 background會變
    2. 1800 < boss血量 < 2500 時 boss會施放閃電
    3. boss血量 < 1000 會開始旋轉
    4. 前兩關敵人的子彈都會自動瞄準玩家